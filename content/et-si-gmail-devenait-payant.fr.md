---
layout:   post
title:    "Et si Gmail devenait payant ?"
date:     2024-03-22
draft:    true
author:   Pierre-Olivier
image:    "https://images.unsplash.com/photo-1603791440384-56cd371ee9a7?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8Z21haWx8ZW58MHx8MHx8&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
#showtoc:  false
---

Imaginez : et si Gmail devenait payant ou fermait votre compte sans préavis, sans raison ? Tous vos e-mails, tous vos contacts, tous vos documents, toutes vos données ne vous sont plus accessibles, du jour au lendemain. Pour pouvoir y accéder de nouveau et continuer d'utiliser les services de Google, vous n'aurez d'autre choix que de souscrire à un abonnement ou vous battre contre le SAV robotisé qui ne fait pas dans les sentiments. En attendant, impossible de retrouver vos courriels importants, de répondre à vos clients, d'utiliser le gestionnaire de mot de passe lié à votre compte, ni même de récupérer vos mots de passe oubliés sur les sites.

Cela ne semble pas prêt d'arriver tant l'entreprise américaine profite des données de chacun de ses utilisateurs pour faire fructifier sa régie publicitaire. Mais cela nous oblige à changer de perspective. Si nous déléguons à une entreprise le soin de gérer nos mails, nous sommes dépendants de son bon vouloir. Si elle décide de nous refuser l'accès à nos données, nous ne pouvons rien faire ; si ses services deviennent payants ou si le prix devient trop élevé, nous avons les pieds et poings liés.

Disposer de son propre nom de domaine est donc primordial pour gagner plus d'indépendance sur Internet. On peut ainsi changer de prestataire sans avoir peur de perdre des courriels, en attendant qu'amis ou client aient bien mis à jour leur carnet d'adresses. Même si l'opération de changement de prestataire peut paraître délicate, car ce n'est pas dans l'intérêt de ces entreprises de faciliter la portabilité des données, cela reste tout à fait possible. Surtout quand on pense à la possibilité de devoir prévenir tous ses contacts que l'on ne peut plus accéder à son ancienne boîte de courrier électronique.

Quand on décide de changer de prestataire, il faut bien sûr une période durant laquelle on mettra en place un répondeur automatique sur son ancienne adresse. Rien de redoutable comparé aux bénéfices qu'apportent le fait d'avoir son nom de domaine.

Nous avons conçu happyDomain pour vous permettre de configurer en toute simplicité votre nom de domaine et les services qui lui sont liés. Reprendre le contrôle sur ses données n'a jamais été aussi simple. (note : bon ça fait peut-être un peu publicitaire, à voir).

---

chose primordiale -> disposer de son propre nom de domaine ... pour pouvoir changer de prestataire sans avoir peur de perdre des courriels en attendant que ses amis ou clients aient bien mis à jour leur carnet d'adresses.

en effet, même si l'opération de changement de prestataire peut sembler délicate (ce n'est pas dans leur intérêt de faciliter la portabilité des données), c'est quelque chose qui reste du domaine du possible comparé à prévenir tout son carnet d'adresse que l'on ne peut plus accéder à son ancienne boîte de courrier électronique.

Bien sûr il faudra une période de transition, en plaçant un répondeur automatique sur son ancienne adresse

Note de rédaction :

* faire référence à la panne d'authentification de Google en décembre 2020.
* faire référence à l'article de blog repéré par Pierre-Olivier narrant la mésaventure d'un utilisateur.
