---
layout:   post
title:    "happyDomain : nos 2 innovations qui simplifient la possession d'un domaine"
date:     2023-10-20
author:   Pierre-Olivier
image:    "/img/happydomain-fr.png"
showtoc:  false
tags:
  - happyDomain
  - simplicité
---

Chez happyDomain, nous [cherchons à simplifier l'usage des noms de domaines]({{< relref "happydomain-simplifions-l-usage-des-noms-de-domaines.fr.md" >}}). Tâche hardue dirons certains, et il est vrai que les 12 RFC établissant le fonctionnement du protocole ne vont pas vraiment dans ce sens, mais l'enjeu pour chacun d'entre-nous est de taille.

Alors voici en quelques mots les deux innovations que nous avons développées pour rendre la possession d'un domaine simple.


## Abstraction du domaine

L'élément clef de notre interface est l'abstraction qu'happyDomain créé à la volée : il regroupe les services de manière efficace.

Par exemple, pour déclarer un serveur Matrix au domaine `example.com`, il faut ajouter dans l'idéal l'enregistrement `SRV` suivant :

```
_matrix._tcp.example.com.  SRV 10 0 8448 matrix.example.com.
```

Outre les divers champs dont on oublie l'ordre et la signification, on voit ici que l'enregistrement est ajouté sur un sous-domaine spécial d'`example.com`. Dans happyDomain, ce service est référencé sous `example.com`, comme on peut s'y attendre, et non pas sous `_matrix._tcp.example.com`.

L'origine de la zone, les différents enregistrements nécessaires pour opérer un serveur de courrier électronique ou bien encore les délégations de noms, tout cela est regroupé et trié. Vous obtenez ainsi une vision plus claire de la zone, moins sujette aux erreurs.


## Transformer les enregistrements DNS en usages concrets

La vie d'une zone DNS est ponctuée par l'ajout de services : ajouter un blog, un serveur de messagerie, un site web, etc, en utilisant la plupart du temps un prestataire comme Google, Over-blog, OVH, Wordpress, Ionos.

C'est là qu'il est parfois compliqué de s'y retrouver : les documentations sont très disparates et les interfaces rustiques des hébergeurs de noms de domaines n'aident pas à faire des documentations faciles d'accès pour le grand public.

Nous réglons cet inconvénient grâce à un formulaire pour chaque prestataire qui :

* récupère automatiquement les informations techniques en accédant au compte de l'utilisateur,
* à défaut, guide l'utilisateur pour récupérer les informations dont il a besoin.

Bien sûr, nous sommes loin d'avoir fait le tour des centaines de services existant sur Internet, mais on y travaille !
