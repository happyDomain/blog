---
layout:   post
title:    "happyDomain : our 2 innovations that simplify domain ownership"
date:     2023-10-20
author:   Pierre-Olivier
image:    "/img/happydomain-fr.png"
showtoc:  false
tags:
  - happyDomain
  - ease
---

At happyDomain, our [aim is to simplify the use of domain names]({{< relref "happydomain-simplifying-the-use-of-domain-names.en.md" >}}).
It's a daunting task, some would say, and it's true that the 12 RFCs establishing how the protocol works don't really go in that direction, but the stakes for each and every one of us are high.

So here, in a nutshell, are the two innovations we've developed to make domain ownership simple.


## Domain abstraction

The key element of our interface is the abstraction that happyDomain creates on the fly: it groups services efficiently.

For example, to declare a Matrix server on the `example.com` domain, you'd ideally add the following `SRV` record:

```
_matrix._tcp.example.com.  SRV 10 0 8448 matrix.example.com.
```

In addition to the various fields, the order and meaning of which has been forgotten, we can see here that the registration has been added to a special sub-domain of `example.com`. In happyDomain, this service is referenced under `example.com`, as you'd expect, and not under `_matrix._tcp.example.com`.

The origin of the zone, the various registrations required to operate an e-mail server or even name delegations are all grouped and sorted.
The result is a clearer, less error-prone view of the zone.


## Turning DNS records into concrete usage

The life of a DNS zone is punctuated by the addition of services: adding a blog, a mail server, a website, etc., usually using a service provider such as Google, Over-blog, OVH, Wordpress or Ionos.

This is where it can be tricky to find your way around: documentation is very disparate, and the rustic interfaces of domain name hosting providers don't help to make documentation easy to access for the general public.

We solve this problem with a form for each provider that:

* automatically retrieves technical information when accessing the user's account,
* alternatively, guides the user to retrieve the information they need.

Of course, we're far from having covered all the hundreds of services available on the Internet, but we're working on it!
