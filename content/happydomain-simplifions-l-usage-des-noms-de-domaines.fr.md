---
layout:   post
title:    "happyDomain : simplifions l'usage des noms de domaine"
date:     2023-09-30
author:   Pierre-Olivier
image:    "/img/happydomain-fr.png"
#showtoc:  false
tags:
  - happyDomain
  - libre
  - vie privée
---

Bienvenue à vous qui découvrez happyDomain !

Notre projet est né d'une idée simple : et si on simplifiait (enfin) l'usage des noms de domaine ?

Parce qu'ils sont un élément clef pour **assurer sa vie privée sur Internet** et parce qu'il n'est **pas toujours simple** de se repérer dans les interfaces parfois obscures des fournisseurs, il nous semblait indispensable de créer **un outil utilisable par tout le monde**, de Monsieur et Madame Tout-le-Monde à l'administrateur système le plus aguerri.

happyDomain permet en effet de gérer **tous les services liés aux noms de domaine** et de paramétrer l'ensemble des entrées d'une zone DNS. <!-- Si vous ne savez pas ce qu'est le DNS, nous avons écrit un article à ce sujet : [vous le trouverez ici]({{< relref "qu-est-ce-que-c-est-le-dns.fr.md" >}}). -->

Parce qu'être présent sur Internet est devenu nécessaire dans notre société hyper-connectée, notre équipe s'engage à vous donner d'une part des outils et d'autre part des garanties pour un Internet plus sain. Voyez plutôt :


## Engagement #1 : une gestion facilitée des noms de domaine

happyDomain est **une interface web moderne**, réalisée par des experts des noms de domaine. Il est possible de l'utiliser **en ligne** ou de l'installer **chez vous**.

Utilisez happyDomain pour gérer vos domaines hébergés chez des prestataires courants ou pour administrer votre propre serveur faisant autorité comme `bind`, `knot`, `powerDNS`, ou encore Microsoft DNS Server, Azure DNS, Route 53, ...

À ce jour, happyDomain dispose des fonctionnalités suivantes :

* L'administration des zones DNS chez Gandi, OVH, et [plus de 40 autres fournisseurs de noms de domaines](https://www.happydomain.org/providers/features).
* L'administration depuis un serveur DNS implémentant le Dynamic DNS (RFC 2136) : bind, knot, PowerDNS, etc.
* Un résolveur pour tester ou déboguer.
* L'historique (encore rudimentaire) pour pouvoir revenir en arrière facilement en cas d'erreur.
* L'affichage, avant propagation, des changements qui seront effectués sur votre domaine, pour limiter vos erreurs.
* Une visualisation « abstraite » de la zone, où les éléments sont regroupés astucieusement ; voir la [présentation du projet]({{< relref "happydomain-nos-2-innovations-qui-simplifient-la-possession-d-un-domaine.fr.md" >}}).


## Engagement #2 : un outil indispensable pour le respect de votre vie privée

Pour être sûr de son indépendance sur Internet, il est nécessaire de disposer de son propre nom de domaine.

Posséder son nom de domaine, c'est s'assurer que tous les services qui y sont liés, comme nos courriels ou notre site internet, sont entre nos mains.

Saviez-vous, par exemple, que les adresses en `@orange.fr` ou `@wanadoo.fr` expirent [6 mois après la résiliation de l'abonnement](https://assistance.orange.fr/assistance-commerciale/la-gestion-de-vos-offres-et-options/resilier-votre-offre/les-conseils-avant-de-resilier-votre-messagerie-mail-orange_71178-72015#onglet4) ? Comment ferez-vous quand vos productions et vos archives seront détruites, sans compter les innombrables services en lignes et autres abonnements rattachés à cette adresse ?

<!-- Qu'adviendra-t-il de votre adresse `@gmail.com` lorsque [l'usage des données personnelles sera régulé par des lois contraignantes]({{< relref "et-si-gmail-devenait-payant.fr.md" >}}) et que Google aura coupé brutalement votre service, par [manque de rentabilité](https://killedbygoogle.com/) ou [d'autres raisons](https://ilya-sher.org/2018/03/23/google-deleted-our-g-suite/) ? -->

Qu'adviendra-t-il de votre adresse `@gmail.com` lorsque l'usage des données personnelles sera régulé par des lois contraignantes et que Google aura coupé brutalement votre service, par [manque de rentabilité](https://killedbygoogle.com/) ou [d'autres raisons](https://ilya-sher.org/2018/03/23/google-deleted-our-g-suite/) ?

Disposer de son nom de domaine permet de se prémunir contre tous ces risques et de devenir vraiment indépendant. Vous pouvez changer de prestataire quand vous le souhaitez. Il n'y aura surtout pas besoin de prévenir tous vos contacts que l'adresse de votre site ou que votre adresse électronique a changé.


## Engagement #3 : un projet open-source pour une totale transparence

happyDomain est un projet libre, sous licence [CeCILL](https://cecill.info/), une licence compatible et comparable à l'AGPL 3.0. Elle garantit son application dans le droit français et international.

Vous pouvez, vous aussi, contribuer au développement d'happyDomain et rendre son usage encore plus facile et agréable pour ses utilisateurs. Nous cherchons ainsi de l'aide pour traduire, programmer, dessiner les interfaces, chercher des bugs, ...

Jetez un œil à notre [dépôt de code](https://git.happydomain.org/happyDomain/), participez [aux choix des prochains développements](https://git.happydomain.org/happyDomain/-/issues/).
