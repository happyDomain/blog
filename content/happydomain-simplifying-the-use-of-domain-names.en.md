---
layout:   post
title:    "happyDomain : simplifions l'usage des noms de domaine"
date:     2023-09-30
author:   Pierre-Olivier
image:    "/img/happydomain-fr.png"
#showtoc:  false
tags:
  - happyDomain
  - free
  - privacy
---

Welcome to happyDomain!

Our project was born of a simple idea: what if we (finally) simplified the use of domain names?

Because domain names are a key element in ensuring privacy on the Internet, and because it's **not always easy** to find your way around the sometimes obscure interfaces of domain name providers, we felt it was essential to create **a tool that could be used by everyone**, from the average Joe and Jane to the most seasoned system administrator.

happyDomain lets you manage **all domain name-related services** and configure all DNS zone entries.
<!-- If you don't know what DNS is, we've written an article on the subject: [you'll find it here]({{< relref "what-is-the-dns.en.md" >}}). -->

Because being present on the Internet has become necessary in our hyper-connected society, our team is committed to giving you both the tools and the guarantees for a healthier Internet. Take a look :


## Commitment #1: easier management of domain names

happyDomain is **a modern web interface** designed by domain name experts. It can be used **online** or installed **at home**.

Use happyDomain to manage your domains hosted by popular providers, or to administer your own authoritative server such as `bind`, `knot`, PowerDNS, or Microsoft DNS Server, Azure DNS, Route 53, ...

To date, happyDomain offers the following features:

* DNS zone administration for Gandi, OVH and [over 40 other domain name providers](https://www.happydomain.org/providers/features).
* Administration from a DNS server implementing Dynamic DNS (RFC 2136): bind, knot, PowerDNS, etc.
* A resolver for testing and debugging.
* History (still rudimentary) to easily go back in time in the event of an error.
* Pre-propagation display of the changes that will be made to your domain, to limit errors.
* An "abstract" visualization of the zone, where elements are cleverly grouped together; see the [project presentation]({{< relref "happydomain-simplifying-the-use-of-domain-names.en.md" >}}).


## Commitment #2: an indispensable tool for respecting your privacy

To be sure of your independence on the Internet, you need your own domain name.

Owning a domain name means ensuring that all related services, such as e-mail or our website, are in our own hands.

Did you know, for example, that `@orange.fr` or `@wanadoo.fr` addresses expire [6 months after subscription termination](https://assistance.orange.fr/assistance-commerciale/la-gestion-de-vos-offres-et-options/resilier-votre-offre/les-conseils-avant-de-resilier-votre-messagerie-mail-orange_71178-72015#onglet4)? What will you do when your productions and archives are destroyed, not to mention the countless online services and other subscriptions attached to this address?

<!-- What will happen to your `@gmail.com` address when [the use of personal data is regulated by restrictive laws]({{< relref "et-si-gmail-devenait-payant.fr.md" >}}) and Google abruptly shuts down your service, for [lack of profitability](https://killedbygoogle.com/) or [other reasons](https://ilya-sher.org/2018/03/23/google-deleted-our-g-suite/)? -->

What will happen to your `@gmail.com` address when the use of personal data is regulated by restrictive laws and Google abruptly shuts down your service, for [lack of profitability](https://killedbygoogle.com/) or [other reasons](https://ilya-sher.org/2018/03/23/google-deleted-our-g-suite/)?

By owning your own domain name, you can protect yourself against all these risks and become truly independent. You can change providers whenever you like. And there's no need to tell all your contacts that your site address or e-mail address has changed.


## Commitment #3: an open-source project for total transparency

happyDomain is a free (as free speech) project, licensed under [CeCILL](https://cecill.info/), a license compatible with and comparable to AGPL 3.0. It guarantees its application in French and international law.

You too can contribute to the development of happyDomain and make its use even easier and more enjoyable for users. We're looking for help with translation, programming, interface design, bug-finding, etc.

Take a look at our [code repository](https://git.happydomain.org/happyDomain/), and help [decide on future developments](https://git.happydomain.org/happyDomain/-/issues/).
