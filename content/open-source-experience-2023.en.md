---
layout:   post
title:    "Our conference at the Open Source Experience"
date:     2023-12-08
author:   Frédéric
image:    "/img/opensourceexperience2023.png"
showtoc:  false
tags:
  - happyDomain
  - conference
  - OSE23
---

The happyDomain team had the incredible opportunity to engage with the vibrant tech community at the [Open Source Experience](https://www.opensource-experience.com/), held on December 7th, 2023, at the Palais des Congrès in Paris.

Our presence at this event was marked by insightful interactions, productive discussions, and the chance to share our vision with a diverse audience. On Thursday, December 7th, we [hosted a conference](https://www.opensource-experience.com/event/#conf-13898) that drew a curious and enthusiastic crowd. The session started at 2:45 pm in room 142, and we were thrilled to see the room buzzing with attendees eager to learn about our innovative approach to DNS and simplicity.

Our team passionately discussed how we aim to redefine the way people think about DNS, making it more accessible and user-friendly. The engagement from the audience was palpable, with numerous questions and lively discussions following our presentation.

For those eager to delve deeper into our presentation, we are excited to [share the complete slideshow](/files/happyDomain%20Open%20Source%20Experience%202023.pdf) on our blog, providing a comprehensive view of our innovative approach to making DNS user-friendly and straightforward.

We encourage you to subscribe to our blog or [to our Mastodon account](https://floss.social/@happyDomain) to stay updated. This way, you won't miss out on this valuable resource, as well as other future updates and insights from our team.

Lastly, we want to extend a heartfelt thank you to everyone who came to meet us at the Open Source Experience. Your enthusiasm and engaging conversations have inspired us, and we are more committed than ever to making DNS synonymous with simplicity.

Stay tuned for more updates, and we hope to see you at our future events!
