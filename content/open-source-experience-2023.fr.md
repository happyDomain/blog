---
layout:   post
title:    "Notre conférence lors de l'Open Source Experience"
date:     2023-12-08
author:   Frédéric
image:    "/img/opensourceexperience2023.png"
showtoc:  false
tags:
  - happyDomain
  - conference
  - OSE23
---

L'équipe d'happyDomain a eu l'incroyable opportunité de s'engager avec la vibrante communauté technologique à l'occasion de l'[Open Source Experience](https://www.opensource-experience.com/), qui s'est tenue le 7 décembre 2023 au Palais des Congrès de Paris.

Notre présence à cet événement a été marquée par des interactions perspicaces, des discussions productives et la possibilité de partager notre vision avec un public diversifié.
Le jeudi 7 décembre, nous avons [animé une conférence](https://www.opensource-experience.com/event/#conf-13898) qui a attiré une foule curieuse et enthousiaste.
La session a débuté à 14h45 dans la salle 142, et nous avons été ravis de voir la salle pleine de participants désireux d'en savoir plus sur notre approche innovante du DNS et de la simplicité.

Notre équipe a discuté avec passion de la manière dont nous visons à redéfinir la façon dont les gens pensent au DNS, en le rendant plus accessible et plus convivial. L'engagement du public était palpable, avec de nombreuses questions et des discussions animées à la suite de notre présentation.

Pour ceux qui souhaitent approfondir notre présentation, nous sommes ravis de [partager le diaporama complet](/files/happyDomain%20Open%20Source%20Experience%202023.pdf) sur notre blog, offrant une vue d'ensemble de notre approche innovante pour rendre le DNS convivial et simple.

Nous vous encourageons à vous abonner à notre blog ou à [notre compte Mastodon](https://floss.social/@happyDomain) pour rester informé.
Ainsi, vous ne manquerez pas cette ressource précieuse, ainsi que d'autres mises à jour et réflexions de notre équipe.

Enfin, nous tenons à remercier chaleureusement tous ceux qui sont venus nous rencontrer à l'Open Source Experience.
Votre enthousiasme et vos conversations engageantes nous ont inspirés, et nous sommes plus que jamais déterminés à faire de DNS un synonyme de simplicité.

Restez à l'écoute pour de nouvelles mises à jour, et nous espérons vous voir lors de nos prochains événements !
