---
layout:   post
title:    "Quels coûts de posséder un nom de domaine ?"
date:     2024-01-29
author:   Pierre-Olivier
image:    "https://images.unsplash.com/photo-1520695625556-c2a7bfe87a2f?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8YmlsbHxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&auto=format&fit=crop&w=700&q=60"
#showtoc:  false
---

Vous souhaitez affirmer votre indépendance sur Internet en possédant votre
propre nom de domaine, mais vous vous heurtez à des informations contradictoires
ou des prix qui varient du simple au triple et peuvent très vite s'envoler.

Vous aimeriez avoir votre nom de domaine bien à vous et ainsi affirmer votre indépednance, mais vous voudriez savoir
combien cela va vous coûter chaque mois. Il y a
de nombreux paramètres à prendre en compte et ce n'est bien sûr pas toujours évident de
bien se projeter. Cet article est là pour vous aider à y voir
plus clair et à lister tous les coûts à prévoir.

Vous allez être agréablement surpris des offres qui existent.

Évidemment, la première dépense concerne le coût
d'acquisition du domaine et sa redevance. Plusieurs cas de figure peuvent se
présenter à vous. Commençaons par les cas les plus
courants.

Quelle que soit votre situation, vous devez considérer tout d'abord le coût
d'achat : il s'agit du prix d'acquisition pour la première année. Ensuite viendra le coût de renouvellement.

## La redevance annuelle

Chaque année, à date anniversaire, vous devrez vous acquitter d'une redevance
pour le renouvellement de votre domaine. Le coût de cette redevance est
relativement stable dans le temps. Vous aurez d'ailleurs généralement la
possibilité de la payer jusqu'à 10 ans en avance. Notez que tous
les bureaux d'enregistrement ajoutent à cette redevance un coût pour leurs
services. N'hésitez donc pas à comparer les offres de différents bureaux
d'enregistrement.

On l'a vu, votre domaine vous coûtera une somme relativement fixe tous les ans une fois en votre possession. Mais comment se déroule l'achat de ce domaine ?

## L'achat

### Domaines disponibles sans condition

Vous avez un domaine en tête et vous vous êtes déjà renseigné pour savoir s'il
était disponible ? Si le bureau d'enregistrement de noms de domaine vous a
indiqué qu'il était disponible sans condition, vous êtes dans le cas le plus
simple : le tarif est connu d'avance, et vous pourrez même parfois payer moins que
la redevance annuelle dans le cadre d'une promotion.

Si vous êtes dans cette situation, vous avez fini cette partie !

### Domaines disponibles sous conditions

Certaines extensions ne sont disponibles que pour des personnes répondant à un
certain nombre de conditions préalables :

Par exemple, il faut pouvoir justifier d'une domiciliation en Europe pour avoir
accès aux domaines en `.eu`. Ou encore être un professionel reconnu, par
exemple être architecte pour accéder aux `.archi`.

Généralement il n'y a pas de papiers à envoyer si votre adresse physique de
contact répond déjà aux critères.

Si vous ne pouvez pas répondre aux conditions d'éligibilité à l'extension que
vous désirez, il va malheureusement vous falloir en trouver une autre, à moins
que vous ne soyez prêt à quelques montages complexes impliquant un contact sur
place, dans le pays concerné, ou dans le milieu concerné.

### Domaines transférables

Enfin, disons un mot des domaines transférables. Il s'agit de domaines déjà
enregistrés et appartenant à quelqu'un.

Plusieurs cas de figure se présentent :

- le domaine est parké : quelqu'un l'a acheté dans le but de le revendre à
  bon prix. Vous pouvez entrer en contact avec son propriétaire pour
  commencer les négociations. Ne prenez pas de risques inconsidérés lors du
  paiement, assurez-vous que l'offre n'est pas une arnaque.

- le domaine est utilisé : il ne sera alors sans doute pas rachetable.

Le plus simple est de se rendre sur le site web du domaine pour se faire une
idée et trouver l'adresse de contact du webmaster.

Pour éviter le parking de domaines, généralement les domaines issus de mots du
dictionnaire, les extensions récentes font payer plus cher certains de leurs
domaines.


## Autres frais du domaines

### Les services optionnels

Afin d'augmenter leurs marges, certains bureaux d'enregistrement n'hésitent pas
à vous inciter à gonfler la facture en ajoutant des prestations souvent
inutiles.

On ne parle pas des prestations d'hébergement de site ou de courriels.

La seule option à envisager est celle permettant de masquer ses informations
personnelles auprès du registre. Car ce sont des informations publiques. Si vous
n'avez pas d'adresse pour votre entreprise ou que vous êtes un particulier,
activez cette option, qui est généralement gratuite.

Toutes les autres options sur votre nom de domaine sont des services avec peu
voire pas de valeur ajoutée. Elles sont parfois même vendues en tentant d'effrayer le
néophyte. Si l'on vous parle d'option payante pour améliorer la protection et
la sécurité, fuyez !

### A-t-on besoin de payer pour l'hébergement de sa zone DNS ?

La zone DNS est le fichier qui décrit les services et sous-domaines de votre
domaine.

La réponse est non. La redevance payée au bureau d'enregistrement couvre les frais facturés
par le registre de l'extension que vous avez choisi pour la redevance annuelle,
ainsi que les services de base, notamment l'hébergement de votre zone DNS.

Notez cependant que, bien que certains bureaux d'enregistrement mettent à
disposition un petit espace de stockage pour votre site web ou la possibilité
d'avoir une adresse électronique, il s'agit d'une offre généralement très
limitée pour vous inciter à profiter des autres offres d'hébergement qu'ils
proposent. Gardez bien en tête, une fois votre domaine en votre possession, que
c'est à vous de choisir vos propres prestataires, pour chacun des services dont
vous souhaitez disposer sur votre domaine.

---

Votre nom de domaine est la première étape de votre indépendance sur Internet.
Son coût est généralement dérisoire : une dizaine d'euros par an.

Mais le domaine seul ne va pas vous procurer beaucoup de services. Il vous
faudra faire appel à d'autres prestataires pour avoir :

- une adresse de messagerie électronique,
- un site web,
- un compte de messagerie instantanée,
- et bien d'autres services...

En fonction de votre situation, ces services pourront être payant selon un
abonnement mensuel, ou être inclus avec votre domaine. Vous pouvez aussi utiliser
les services d'une association.

Consultez les articles dédiés pour voir quelles offres vous conviendraient le
mieux et comment happyDomain pourra vous aider à les mettre en place.
